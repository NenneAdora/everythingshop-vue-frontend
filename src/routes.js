import Homepage from './Homepage.vue';
import Auth from './Auth.vue';
import Products from './Products.vue';
import Checkout from './Checkout.vue';
import Cart from './Cart.vue';
import Confirmation from './Confirmation.vue';


export const routes = [
    { 
        path: '/', 
        component: Homepage 
    },
    { 
        path: '/login', 
        component: Auth 
    },
    { 
        path: '/products', 
        component: Products 
    },
    { 
        path: '/cart', 
        component: Cart 
    },
    { 
        path: '/checkout', 
        component: Checkout,
        name: "checkout"
        
    },
    { 
        path: '/confirmed', 
        component: Confirmation,
        beforeEnter(to, from, next){
            if(from.name != "checkout"){
                return next("/"); 
            }
            next();
        }

    },
    { 
        path: '*', 
        component: { template: '<h1>Page Not Found</h1>'} 
    }
];