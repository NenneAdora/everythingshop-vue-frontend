import Vue from 'vue';
import App from './App.vue';
import Navigation from './Navigation.vue';
import Footer from './Footer.vue';
import VueRouter from 'vue-router';
import SimpleNavigation from './SimpleNavigation.vue';
import { routes } from './routes'; 
import VueResource from 'vue-resource';
import VueCurrencyFilter from 'vue-currency-filter';
import VueLocalStorage from 'vue-localstorage';
import VueSession from 'vue-session';

export const authService = { isLoggedIn : false };

Vue.component('everything-shop-navigation', Navigation);
Vue.component('everything-shop-footer', Footer);
Vue.component('everything-shop-simple-navigation', SimpleNavigation);

Vue.use(VueRouter);
Vue.use(VueResource);
Vue.use(VueSession);
Vue.use(VueCurrencyFilter,
  {
    symbol : '₦',
    thousandsSeparator: ',',
    fractionCount: 2,
    fractionSeparator: '.',
    symbolPosition: 'front',
    symbolSpacing: true
  }
);
Vue.use(VueLocalStorage, {
  name: 'everything-shop-ls',
  bind: true 
});

const router = new VueRouter({
  routes: routes,
  mode: 'history'

});


new Vue({
  el: '#app',
  render: h => h(App),
  router: router,
})
